({
    clickCreateItem: function(component, event, helper){
        var validItem = component.find('itemform').reduce(
            function (validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid;
            }, 
            true
        );
        if(validItem){
            var newItem = component.get("v.newItem");
            console.log('create item > ', JSON.stringify(newItem));
            helper.createItem(component, newItem);
            component.set("v.newItem",{'sobjectType':'Camping_Item__c',
                'Name': '',
                'Quantity__c': 0,
                'Price__c': 0,
                'Packed__c': false});
        }
    },
    createItems: function(component, item) {
        var theItems = component.get("v.items");
        var newItem = JSON.parse(JSON.stringify(item));
        
        theItems.push(newItem);
        console.log(theItems);
        component.set("v.items", theItems);
    },
    
    doInit: function(component, event, helper) {
        var action = component.get("c.getItems");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                console.log("state success = "+ response.getReturnValue());
                component.set("v.items", response.getReturnValue());
            } else {
                console.log("failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    }
})