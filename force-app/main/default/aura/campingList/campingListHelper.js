({
    createItem: function(component, item) {
        //save item into salesforce db
        var action = component.get("c.saveItem");
        action.setParams({
            "campingItem": item
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS") {
                var items = component.get("v.items");
                items.push(response.getReturnValue());
                component.set("v.items", items);
            }
        });
        $A.enqueueAction(action);
        
        // save item via client-side application
        //var theItems = component.get("v.items");
        //var newItem = JSON.parse(JSON.stringify(item));
        
        //theItems.push(newItem);
        //console.log(theItems);
        //component.set("v.items", theItems);
    }
})