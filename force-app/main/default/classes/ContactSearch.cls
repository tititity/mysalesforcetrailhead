public class ContactSearch {
    public static List<Contact> searchForContacts(String lastName, String postalCode) {
        List<Contact> contacts = [SELECT Contact.Id, Contact.Name FROM Contact
                                 WHERE LastName=:lastName AND MailingPostalCode=:postalCode ];
        System.debug(contacts);
        return contacts;	
    }
}