public class StringArrayTest {
    public static List<String> generateStringArray (Integer param) {
        System.debug('>>>>> paramnumber is '+ param);
        List<String> strArr = new List<String>();
        for(Integer i = 0; i < param; i++) {
            strArr.add('Test '+ i);
        }
        System.debug(strArr);
        return strArr;
    }
}